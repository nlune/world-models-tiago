#!/usr/bin/env python
import numpy as np
import csv 
import math
from torchvision import transforms
from PIL import Image
import os
import matplotlib as plt
import scipy.misc



def process_laser_data(csvfile, newfile = "laser_counts2.csv"):
    # count the laser data smaller than 0.4
    # mark as terminal if count greater than 50

    f = open(csvfile, "r") 
    fn = open(newfile, "wb")
    reader = csv.reader(f)
    writer = csv.writer(fn)

    next(reader) # skip header
    counts = []

    for row in reader: 
        count = 0 
        col14 = row[14][1:-1]
        col14 = col14.split(',')
        for i in col14: 

            i = float(i)
            if math.isnan(i):
                i = 0.0
            if math.isinf(i):
                i = 25.0

            if i <= 0.4: 
                count+=1 
                
           
        print(count)
        counts.append([count])

    writer.writerows(counts)


    f.close()
    fn.close()



def transform_augment_data(img_dirs, terminals, cmd_vel_csvs, save_original=True, augment=True):
    # lists of dirs in order ; orig image 480 x 640
    # augmenting horizontal flipped images and random crop imgs -- try grayscale later

    original_transform = transforms.Compose([#transforms.Grayscale(),
                                            transforms.Resize((64,64))])
                              
    aug1_transform = transforms.Compose([ transforms.RandomHorizontalFlip(1),
                                       #transforms.Grayscale(),
                                        transforms.Resize((64,64))])
                             

    # aug2_transform = transforms.Compose([ # transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0),
    #                                     transforms.Grayscale(),
    #                                     transforms.Resize((64,64))])
                    


    images = []
    aug1_imgs = []
    # aug2_imgs = []
    dones = []
    rewards = []
    actions = []


    # for laser
    for cs in terminals: 
        fi = open(cs, "r") 
        reader = csv.reader(fi)


        for row in reader: 
            if float(row[0]) >  50: # set threshold for laser counts 
                dones.append(1)
                # print(1)
                rewards.append(-10)
            else: 
                dones.append(0)
                # print(0)
                rewards.append(1)

        print("finished dones")



    # for cs in terminals: 
    #     fi = open(cs, "r") 
    #     reader = csv.reader(fi)
    #     i = 0
    #     for row in reader: 
    #         if int(row[0]) == 1: # set threshold for laser counts 
    #             dones.append(1)
    #             rewards.append(-10)
                
    #             i+=1
    #         else: 
    #             dones.append(0)
    #             rewards.append(1)
    #             i+=1

    print("finished dones")


    for c in cmd_vel_csvs: 
        cv = open(c, "r") 
        read = csv.reader(cv)

        next(read)

        for line in read:
            # print(line[9], line[10], line[11], line[13], line[14], line[15])
            vel = [line[9], line[10], line[11], line[13], line[14], line[15]]
            vel = [float(v) for v in vel ]
            actions.append(vel)
        
    print("finished actions")


    for d in img_dirs: 
        for filename in sorted(os.listdir(d)):

                img = Image.open(os.path.join(d,filename))
                

                if save_original:
                    img1 = original_transform(img)
                    images.append(img1)

                if augment:
                    img2 = aug1_transform(img)
                    aug1_imgs.append(img2) 
                    # img3 = aug2_transform(img)
                    # aug2_imgs.append(img3)

        print("finished image transforms")
       

    total_imgs = images + aug1_imgs 
    total_imgs = [np.array(img) for img in total_imgs]

    # duplicate values based on how many times augmented images
    times = len(total_imgs) / len(dones)
    
    total_dones = times*dones
    # print(total_dones) 
    total_rewards = times*rewards
    total_actions = times*actions

    print(len(total_imgs), len(total_actions), len(total_dones), len(total_rewards))

    # # debugging
    for i, img in enumerate(total_imgs):
        if total_dones[i] == 1:
            print(i)
            scipy.misc.imsave('debug/outfile' + str(i) + '.jpg', img)



    np.savez("testdata.npz", 
                images=np.array(total_imgs),
                dones = np.array(total_dones),
                rewards = np.array(total_rewards),
                actions=np.array(total_actions))




if __name__ == "__main__": 
    # process_laser_data('testdata2/synced_laser.csv')
    # transform_augment_data(["120719img1", "120719img2", "120719img3", "120719img4", "120719img5"], ["120719dat1/_synced_dpth.csv", 
    # "120719dat2/_synced_dpth.csv","120719dat3/_synced_dpth.csv","120719dat4/_synced_dpth.csv","120719dat5/_synced_dpth.csv"], 
    # ["120719dat1/_synced_cmdvel.csv","120719dat2/_synced_cmdvel.csv","120719dat3/_synced_cmdvel.csv","120719dat4/_synced_cmdvel.csv",
    # "120719dat5/_synced_cmdvel.csv"])
    transform_augment_data(["testimages", "testimages2"], ["testdata/laser_counts.csv", "testdata2/laser_counts2.csv"], 
    ["testdata/synced_cmdvel.csv", "testdata2/synced_cmdvel.csv"], augment=False)


            

