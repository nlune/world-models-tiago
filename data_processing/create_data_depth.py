#!/usr/bin/env python
import numpy as np
import csv 
import math
from torchvision import transforms
from PIL import Image
import os
import matplotlib as plt
import scipy.misc
import cv2


def transform_augment_data(depth_arrays, terminals, cmd_vel_csvs):
    # depth arrays and terminals npy files
    actions = []
    depths = []
    dones = []
    rewards = []

    for d in depth_arrays:
        print("creating depths array...", d)
        depth_arr = np.load(d)
        # print("depth arr shape ", depth_arr.shape)
        for da in depth_arr:
            arr = cv2.resize(da, (64,64))
            arr = np.expand_dims(arr, axis=0)
            arr = arr
            depths.append(arr)

    print(np.array(depths).max(), np.array(depths).shape)


    for t in terminals:
        print("creating temrinal array ...", t)
        term_arr = np.load(t)
        for d in term_arr:
            if d == 1: 
                dones.append(1)
                rewards.append(-10)
            else:
                dones.append(0)
                rewards.append(1)
                

    for c in cmd_vel_csvs: 
        print("creating actions array....", c)
        cv = open(c, "r") 
        read = csv.reader(cv)

        next(read)

        for line in read:
            vel = [line[9], line[10], line[11], line[13], line[14], line[15]]
            vel = [float(v) for v in vel ]
            actions.append(vel)
        
    print("finished actions")

    depths = np.array(depths)
    ones_0 = []
    ones_ = []
    zeros_0 = []
    zeros_ = []
    for i,v in enumerate(dones):
        if v == 1: 
            ones_0.append(np.count_nonzero(depths[i] == 0))
            ones_.append(np.count_nonzero((depths[i] > 0) & (depths[i] < 0.6)))

        else:
            zeros_0.append(np.count_nonzero(depths[i] == 0))
            zeros_.append(np.count_nonzero((depths[i] > 0) & (depths[i] < 0.6)))

    ones_0 = np.array(ones_0)
    ones_ = np.array(ones_)
    zeros_0 = np.array(zeros_0)
    zeros_ = np.array(zeros_)
    print("ones 0 counts", ones_0.min(), ones_0.max(), "  min counts", ones_.min(), ones_.max())
    print("zeros 0 counts", zeros_0.min(), zeros_0.max(), "  min counts", zeros_.min(), zeros_.max())
    


    np.savez("depthdata.npz", 
                images=np.array(depths),
                dones = np.array(dones),
                rewards = np.array(rewards),
                actions=np.array(actions))




if __name__ == "__main__": 
    # process_laser_data('testdata2/synced_laser.csv')
    # transform_augment_data(["120719img1", "120719img2", "120719img3", "120719img4", "120719img5"], ["120719dat1/_synced_dpth.csv", 
    # "120719dat2/_synced_dpth.csv","120719dat3/_synced_dpth.csv","120719dat4/_synced_dpth.csv","120719dat5/_synced_dpth.csv"], 
    # ["120719dat1/_synced_cmdvel.csv","120719dat2/_synced_cmdvel.csv","120719dat3/_synced_cmdvel.csv","120719dat4/_synced_cmdvel.csv",
    # "120719dat5/_synced_cmdvel.csv"])

    # transform_augment_data(["depthdat1bag/depth_arrays.npy", "depthdat2bag/depth_arrays.npy", "depthdat3bag/depth_arrays.npy", "depthdat4bag/depth_arrays.npy",
    # "depthdat5bag/depth_arrays.npy", "depthdat6bag/depth_arrays.npy", "depthnldat1bag/depth_arrays.npy", "depthnldat2bag/depth_arrays.npy", "depthnldat3bag/depth_arrays.npy",
    # "depthnldat4bag/depth_arrays.npy", "depthnldat5bag/depth_arrays.npy"], ["depthdat1bag/terminals.npy", "depthdat2bag/terminals.npy", "depthdat3bag/terminals.npy",
    # "depthdat4bag/terminals.npy", "depthdat5bag/terminals.npy", "depthdat6bag/terminals.npy", "depthnldat1bag/terminals.npy", "depthnldat2bag/terminals.npy",
    # "depthnldat3bag/terminals.npy","depthnldat4bag/terminals.npy","depthnldat5bag/terminals.npy"], 
    # ["depthdat1bag/_synced_cmdvel.csv", "depthdat2bag/_synced_cmdvel.csv", "depthdat3bag/_synced_cmdvel.csv","depthdat4bag/_synced_cmdvel.csv","depthdat5bag/_synced_cmdvel.csv",
    # "depthdat6bag/_synced_cmdvel.csv", "depthnldat1bag/_synced_cmdvel.csv", "depthnldat2bag/_synced_cmdvel.csv","depthnldat3bag/_synced_cmdvel.csv","depthnldat4bag/_synced_cmdvel.csv",
    # "depthnldat5bag/_synced_cmdvel.csv"])

    transform_augment_data(['depthtestbag/depth_arrays.npy', 'depthtest2bag/depth_arrays.npy'], ['depthtestbag/terminals.npy', 'depthtest2bag/terminals.npy'], 
    ['depthtestbag/_synced_cmdvel.csv', 'depthtest2bag/_synced_cmdvel.csv'])

            

