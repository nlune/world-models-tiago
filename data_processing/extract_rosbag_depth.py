# Python script for extract information from a rosbag
#
# info class bag : http://docs.ros.org/jade/api/rosbag/html/python/rosbag.bag.Bag-class.html
#
# For recording a rosbag :
# rosbag record -O tiago.bag /xtion/rgb/image_raw /scan /mobile_base_controller/cmd_vel --duration=300

# python extract_rosbag.py name.bag
# change folders for data and image

import os
import string
import csv
import rosbag
import sys
import cv2
import numpy as np
import sys

import ros_numpy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

#give your rosbag path in argument
for bagname in sys.argv[1:]:

	bag = rosbag.Bag(bagname)

        print(bagname)

        bagdirname = bagname.replace('.','')


        # Returns the number of messages in the bag. Can be filtered by Topic
        num_msg= bag.get_message_count()

        # Coallates info about the type and topics in the bag.
        topics = bag.get_type_and_topic_info()[1].keys()


        # Returns the start time of the bag.
        start_time = bag.get_start_time()

        # Returns the end time of the bag.
        end_time = bag.get_end_time()

        # print message type and text information
        types = {}
        for topic, msg, t in bag.read_messages():
                if not msg._type in types:
                        types[msg._type] = msg._full_text
        for t in types:
                print("Message type:", t)
                print("Message text:")
                print(types[t])
        
        bridge = CvBridge()
        count=0
        terminals = []
        depth_arrays = []
        # select your topic and do something
        for topic, msg, t in bag.read_messages(topics):
                print(topic)
                dirname = 'depth' + bagdirname + "/"
                
                if not os.path.exists(dirname):
                        os.makedirs(dirname)

                filename = dirname + string.replace(topic, '/', '_') + '.csv'
                
                if topic == "/synced_dpth":
                        # save as npy for z dim of point cloud, and also terminal values
                        cloud = ros_numpy.point_cloud2.pointcloud2_to_array(msg)
                        cloud_z = np.nan_to_num(cloud['z'])

                        depth_arrays.append(cloud_z)
         

                        if np.count_nonzero(cloud_z == 0) > 290000:
                                terminals.append([1])
                        elif (np.count_nonzero((cloud_z < 0.6) & (cloud_z > 0))) > 1000:
                                terminals.append([1])
                                        
                        else: 
                                terminals.append([0])
        
                        


                if topic == "/synced_cmdvel":
                        with open(filename, 'w+') as csvfile:
                                filewriter = csv.writer(csvfile, delimiter = ',')
                                firstIteration = True   #allows header row
                                for subtopic, msg, t in bag.read_messages(topic):       # for each instant in time that has data for topicName
                                        #parse data from this instant, which is of the form of multiple lines of "Name: value\n"
                                        #       - put it in the form of a list of 2-element lists
                                        msgString = str(msg)
                                        msgList = string.split(msgString, '\n')
                                        instantaneousListOfData = []
                                        for nameValuePair in msgList:
                                                splitPair = string.split(nameValuePair, ':')
                                                for i in range(len(splitPair)): #should be 0 to 1
                                                        splitPair[i] = string.strip(splitPair[i])
                                                instantaneousListOfData.append(splitPair)
                                        #write the first row from the first element of each pair
                                        if firstIteration:      # header
                                                headers = ["rosbagTimestamp"]   #first column header
                                                for pair in instantaneousListOfData:
                                                        headers.append(pair[0])
                                                filewriter.writerow(headers)
                                                firstIteration = False
                                        # write the value from each pair to the file
                                        values = [str(t)]       #first column will have rosbag timestamp
                                        for pair in instantaneousListOfData:
                                                if len(pair) > 1:
                                                        values.append(pair[1])
                                        filewriter.writerow(values)    


        np.save(dirname + "terminals", np.array(terminals))
        np.save(dirname + "depth_arrays", np.array(depth_arrays))

        bag.close()
