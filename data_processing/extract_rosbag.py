# Python script for extract information from a rosbag
#
# info class bag : http://docs.ros.org/jade/api/rosbag/html/python/rosbag.bag.Bag-class.html
#
# For recording a rosbag :
# rosbag record -O tiago.bag /xtion/rgb/image_raw /scan /mobile_base_controller/cmd_vel --duration=300
# python extract_rosbag.py name.bag
# change folders for data and image

import os
import string
import csv
import rosbag
import sys
import cv2
import numpy as np
import sys

import ros_numpy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

#give your rosbag path in argument
for bagname in sys.argv[1:]:

	bag = rosbag.Bag(bagname)

# Returns the number of messages in the bag. Can be filtered by Topic
num_msg= bag.get_message_count()

# Coallates info about the type and topics in the bag.
topics = bag.get_type_and_topic_info()[1].keys()


# Returns the start time of the bag.
start_time = bag.get_start_time()

# Returns the end time of the bag.
end_time = bag.get_end_time()

# print message type and text information
types = {}
for topic, msg, t in bag.read_messages():
        if not msg._type in types:
            types[msg._type] = msg._full_text
for t in types:
    print("Message type:", t)
    print("Message text:")
    print(types[t])

bridge = CvBridge()
count=0
counts = []
# select your topic and do something
for topic, msg, t in bag.read_messages(topics):
        print(topic)
	filename = 'testdepthvals'+ '/' + string.replace(topic, '/', '_') + '.csv'
        
        if topic == "/synced_dpth":
                cloud = ros_numpy.point_cloud2.pointcloud2_to_array(msg)
                
                with open(filename, 'w+') as csvfile:
                        filewriter = csv.writer(csvfile, delimiter = ',')
                        tot_vals = np.multiply(*cloud.shape)
                        print(tot_vals)
                        close_vals = np.count_nonzero(cloud['z'] < 0.4)
                        if np.all(np.isnan(cloud['z'])) or (np.count_nonzero(np.isnan(cloud['z'])) > (tot_vals - close_vals - 100)):
                                counts.append([1])
                        elif (np.count_nonzero(cloud['z'] < 0.4)) > 1000:
                                counts.append([1])
                                print('min', cloud['z'][~np.isnan(cloud['z'])].min())
                        else: 
                                counts.append([0])
                        # if len(counts) > 59 and len(counts) < 89 : 
                        #         print(len(counts), "counting the holes", np.all(np.isnan(cloud['z'])), np.count_nonzero(np.isnan(cloud['z'])), np.count_nonzero(cloud['z'] < 0.4)) 
                        # if len(counts) == 90:
                        #         sys.exit()
                        
                        filewriter.writerows(counts)
                


        if topic == "/synced_cam":
            cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")

            cv2.imwrite(os.path.join("testdepthvals/images", "frame%06i.png" % count), cv_img)
            print("Wrote image %i" % count)
            count+=1


        # if topic == "/synced_laser":
	# 	with open(filename, 'w+') as csvfile:
	# 		filewriter = csv.writer(csvfile, delimiter = ',')
	# 		firstIteration = True	#allows header row
	# 		for subtopic, msg, t in bag.read_messages(topic):	# for each instant in time that has data for topicName
	# 			#parse data from this instant, which is of the form of multiple lines of "Name: value\n"
	# 			#	- put it in the form of a list of 2-element lists
	# 			msgString = str(msg)
	# 			msgList = string.split(msgString, '\n')
	# 			instantaneousListOfData = []
	# 			for nameValuePair in msgList:
	# 				splitPair = string.split(nameValuePair, ':')
	# 				for i in range(len(splitPair)):	#should be 0 to 1
	# 					splitPair[i] = string.strip(splitPair[i])
	# 				instantaneousListOfData.append(splitPair)
	# 			#write the first row from the first element of each pair
	# 			if firstIteration:	# header
	# 				headers = ["rosbagTimestamp"]	#first column header
	# 				for pair in instantaneousListOfData:
	# 					headers.append(pair[0])
	# 				filewriter.writerow(headers)
	# 				firstIteration = False
	# 			# write the value from each pair to the file
	# 			values = [str(t)]	#first column will have rosbag timestamp
	# 			for pair in instantaneousListOfData:
	# 				if len(pair) > 1:
	# 					values.append(pair[1])
	# 			filewriter.writerow(values)            

        if topic == "/synced_cmdvel":
               with open(filename, 'w+') as csvfile:
                        filewriter = csv.writer(csvfile, delimiter = ',')
                        firstIteration = True   #allows header row
                        for subtopic, msg, t in bag.read_messages(topic):       # for each instant in time that has data for topicName
                                #parse data from this instant, which is of the form of multiple lines of "Name: value\n"
                                #       - put it in the form of a list of 2-element lists
                                msgString = str(msg)
                                msgList = string.split(msgString, '\n')
                                instantaneousListOfData = []
                                for nameValuePair in msgList:
                                        splitPair = string.split(nameValuePair, ':')
                                        for i in range(len(splitPair)): #should be 0 to 1
                                                splitPair[i] = string.strip(splitPair[i])
                                        instantaneousListOfData.append(splitPair)
                                #write the first row from the first element of each pair
                                if firstIteration:      # header
                                        headers = ["rosbagTimestamp"]   #first column header
                                        for pair in instantaneousListOfData:
                                                headers.append(pair[0])
                                        filewriter.writerow(headers)
                                        firstIteration = False
                                # write the value from each pair to the file
                                values = [str(t)]       #first column will have rosbag timestamp
                                for pair in instantaneousListOfData:
                                        if len(pair) > 1:
                                                values.append(pair[1])
                                filewriter.writerow(values)    

bag.close()
