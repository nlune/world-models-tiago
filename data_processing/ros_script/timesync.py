#!/usr/bin/env python
import struct
import rospy
import ros_numpy
import numpy as np
from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud2
from geometry_msgs.msg import TwistStamped
from sensor_msgs.msg import LaserScan
from message_filters import ApproximateTimeSynchronizer, Subscriber


global sync_cmdvel; sync_cmdvel = TwistStamped() 
global sync_img; sync_img = Image()
global point_cloud; point_cloud = PointCloud2()
# global depth_img; depth_img = Image()


def callback(cmd_vel, img, pt_cloud):
    # record rosbag here
    global sync_cmdvel
    sync_cmdvel = cmd_vel 

    global sync_img
    sync_img  = img

    global point_cloud
    point_cloud = pt_cloud

    print("got data")


cmd_vel_sub = Subscriber("/mobile_base_controller/cmd_vel_out", TwistStamped) # name of topic
camera_sub = Subscriber("/xtion/rgb/image_raw", Image) 
depth_sub = Subscriber("/xtion/depth/points", PointCloud2)

ats = ApproximateTimeSynchronizer([cmd_vel_sub, camera_sub, depth_sub], queue_size=30, slop=1) #slop is error btwn times of the msgs
ats.registerCallback(callback)

pub_cmdv = rospy.Publisher('synced_cmdvel', TwistStamped, queue_size=30)
pub_im = rospy.Publisher('synced_cam', Image, queue_size=30)
pub_dpt = rospy.Publisher('synced_dpth', PointCloud2, queue_size= 30)

rospy.init_node('synced_data', anonymous=True)

rate = rospy.Rate(10) 

while not rospy.is_shutdown():
    pub_cmdv.publish(sync_cmdvel)
    pub_im.publish(sync_img)
    pub_dpt.publish(point_cloud)

    rate.sleep()
