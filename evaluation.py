#!/usr/bin/env python
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torchvision import transforms
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image
from tqdm import tqdm
import argparse

from models.vae import VAE
from models.mdnrnn import MDN_RNN, MDN_RNN_Cell
from utils.atari_loaders import RolloutSequenceDataset


device = torch.device( "cuda" if torch.cuda.is_available() else "cpu")

def encode_obs(obs, next_obs=None, getboth=True):
    with torch.no_grad():
        zseq = [vae.forward_encode(seq).detach() for seq in obs] # [bs x seqlen], type [list, Tensor]
        z = torch.stack(zseq)
        del zseq

        if getboth:
            zseq_next = [vae.forward_encode(seq).detach() for seq in next_obs]
            z_next = torch.stack(zseq_next)
            del zseq_next

            return z, z_next
    return z

def compare_ds(ds, terminals, soft=False, steps=5, threshold = 0.5):
    # return precision (true ds / all predicted ds), recall (true ds / all ds), and accuracy (correct / total)
    with torch.no_grad():
        if torch.is_tensor(ds):
            ds = ds.cpu().numpy()
        else:
            ds = np.asarray(ds)

        eps=1e-10


        total_ones = np.count_nonzero(terminals) + eps
        num_predicted_ones = np.count_nonzero(ds >= threshold) + eps

        # predicted_ones = ds[terminals>0] >= 0.5
        correct_ones = np.count_nonzero(ds[terminals==1] >= threshold)
        correct_ones_pred = correct_ones

        if soft: # increase count of correct d prediction if predicted for steps prior to real d
            ones_idx = np.transpose(np.nonzero(terminals))
            for idx in ones_idx:
                if len(idx) == 2:
                    i, j = idx 
                    if j-1 >=0 and terminals[i, j-1] == 0: 
                        for s in range(steps + 1): 
                            if j-s >=0 and terminals[i, j-s] == 0 and ds[i, j-s] > threshold:
                                correct_ones_pred += 1 # - (s/steps)
                else: 
                    if idx-1 >=0 and terminals[idx-1] == 0:
                        for s in range(steps + 1): 
                            if idx-s >=0 and terminals[idx-s] == 0 and ds[idx-s] > threshold:
                                correct_ones_pred += 1 # - (s/steps)
                        

        total = np.prod(ds.shape)
        correct_zeros = np.count_nonzero(ds[terminals==0] <= threshold)
     
        if total_ones == 0 + eps : 
            return None, None, (correct_ones + correct_zeros)/ total, total_ones

    return correct_ones_pred/float(num_predicted_ones), correct_ones/float(total_ones), (correct_ones + correct_zeros)/ float(total), total_ones






