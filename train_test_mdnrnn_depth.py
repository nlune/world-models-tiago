#!/usr/bin/env python
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image
from tqdm import tqdm
import argparse

import evaluation as evaluate
from models.vae import VAE
from models.mdnrnn import MDN_RNN, MDN_RNN_Cell

from utils.get_loader import get_loader
from utils.data_loader import CustomSeqDataset

'''add argparser
-t -bs  -e -sm --model_path(for both)
'''

# config
training = False
use_saved_mdnrnn = True
mdrnn_location = 'saved/model_ckpts/twm_mdrnn_depthrelu.pt'
learning_rate=0.001

zdim = 32
epochs = 50

examine_dones = False
examine_mispredicted_frames = False

# training
use_step_weights = False
bce_weights = None #[0.07, 0.93] 
soft_pra = True
model_save_name = 'twm_mdrnn_depthrelu3.pt'
tagname = 'twm_mdrnn_depthrelu'

if training:
    print("train ", tagname, '; Model best loss saved as: ', model_save_name )
else: 
    print("testing ", mdrnn_location)


chosen_batch = 5 # for saving img
img_save_path = 'saved/real_frame/'
save_path = 'saved/model_ckpts/'
savereal = False # save real images to compare prediction



device = torch.device( "cuda" if torch.cuda.is_available() else "cpu")

# instantiate models
vae = VAE(latent_dim=zdim).to(device) # load trained model

action_size = 6
batch_size=16
seq_len = 32
model_path = 'saved/model_ckpts/twm_vaedepth_relu.pt' # set model path


dataset = CustomSeqDataset('datasets/tiago_depth/', seq_len)
lengths = [int(len(dataset)*0.85), int(len(dataset)*0.15)]
if sum(lengths) != len(dataset):
    lengths[1] += 1

dataset_train, dataset_val = random_split(dataset, lengths)

train_loader = DataLoader(dataset_train, batch_size=batch_size, num_workers=0, shuffle=True, drop_last=True)

val_loader = DataLoader(dataset_val, batch_size=batch_size, num_workers=0, shuffle=True, drop_last=True)


test_dataset = CustomSeqDataset('datasets/tiago_depth_test/', seq_len)
test_loader = DataLoader(test_dataset, batch_size=batch_size, num_workers=0, shuffle=True, drop_last=True)


mdrnn = MDN_RNN(batch_size, seq_len, action_size=action_size, z_dim=zdim).to(device)

ckpt= torch.load(model_path)
vae.load_state_dict(ckpt['model_state_dict'])
print("Loaded VAE at epoch {} with test loss {} ".format(ckpt['epoch'], ckpt['testloss']))

optimizer = optim.Adam(mdrnn.parameters(), lr=learning_rate)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.3, patience=1, verbose=True)

if use_saved_mdnrnn:
    model_path = mdrnn_location  # set model path
    ckpt= torch.load(model_path)
    mdrnn.load_state_dict(ckpt['model_state_dict'])
    optimizer.load_state_dict(ckpt['optimizer_state_dict'])
    # scheduler.load_state_dict(ckpt['scheduler'])
    print("Loaded MDN_RNN at epoch {} with test loss {}".format(ckpt['epoch'], ckpt['testloss']))



toim = transforms.ToPILImage()



def encode_obs(obs, next_obs=None, getboth=True):
    with torch.no_grad():
        zseq = [vae.forward_encode(seq).detach() for seq in obs] # [bs x seqlen], type [list, Tensor]
        z = torch.stack(zseq)
        del zseq

        if getboth:
            zseq_next = [vae.forward_encode(seq).detach() for seq in next_obs]
            z_next = torch.stack(zseq_next)
            del zseq_next

            return z, z_next
    return z


def weighted_BCE(output, target, weights=None, step_weights=use_step_weights, steps = 5, threshold = 0.5):
    eps = 1e-40
    if weights is not None:
        assert len(weights) == 2, 'only 2 weights for BCE'
        
        loss = weights[1] * (target * torch.log(output + eps)) + \
            weights[0] * ((1 - target) * torch.log(1 - output + eps))
        # weight loss as step
        if step_weights: 
            ones_idx = np.transpose(np.nonzero(target.cpu().numpy()))

            for idx in ones_idx:
                i, j = idx 
                for s in range(steps): 
                    if j-s >=0 and target[i, j-s] == 0 and output[i, j-s] > threshold: 
                        # loss[i, j - s] = (s/steps)*weights[1] * target[i,j-s] * torch.log(output[i, j-s]) + \
                        #     (s/steps)*weights[0] * ((1 - target[i,j-s]) * torch.log(1 - output[i, j-s]))
                        loss[i, j-s] = (s/steps)*weights[0] * ((1 - target[i,j-s]) * torch.log(1 - output[i, j-s]))
       
    else:
        loss = target * torch.log(output + eps) + (1 - target) * torch.log(1 - output + eps)

    return torch.neg(torch.mean(loss)) 


def get_total_loss(model, pis, sigmas, mus, rs, ds, z_next, terminal, reward=None, include_rewards=False):

    m_loss = model.get_loss(z_next, pis, mus, sigmas)

    if include_rewards:
        r_loss = F.mse_loss(rs, reward)
        scale = model.z_dim + 2
    else:
        r_loss = 0
        scale = model.z_dim + 1

    pred_loss = r_loss 

    total_loss = (m_loss + pred_loss) / scale 

    return total_loss




params = sum(p.numel() for p in mdrnn.parameters() if p.requires_grad)
print('model trainable params: ', params)

vae.train(False)

def train(epoch,include_rewards=False):
    mdrnn.train()

    print('>>>training...')
 

    disp_loss = 0
    done_loss = 0
    pbar = tqdm(total=len(train_loader.dataset), desc="Epoch {}".format(epoch))
    li = 0
    precisions = []
    recalls = []
    accuracies = []
    terminal_ones = 0
    for data in train_loader:

        li += 1

        obs, actions, reward, terminal, next_obs = [arr.to(device) for arr in data]
        
        del data

        # examine images with death
        if examine_dones:
            real_batch = obs[chosen_batch]
            ds_batch = terminal[chosen_batch].cpu().numpy()

            if any(ds_batch == 1):
                for j in range(seq_len):
                    realimg = toim(real_batch[j].detach().cpu())
                    imgr = realimg.resize((480, 720), Image.ANTIALIAS)

                    if ds_batch[j] == 1.0: 
                        imgr.save(img_save_path  + 'real' + str(li) + '_' + str(j) + '_DEATH' + '.jpg')
                    else:
                        imgr.save(img_save_path  + 'real' + str(li) + '_' + str(j) + '.jpg')

                    print('saved ', li, ', ', j)

        z, z_next = encode_obs(obs, next_obs)
        if len(z.shape) != len(actions.shape): 
            actions = actions.unsqueeze(-1)
        inputs = torch.cat([z, actions], dim=-1)

        pis, sigmas, mus, rs = mdrnn.forward(inputs)

        ds = np.empty_like(rs.detach().cpu())
        z_pred = mdrnn.get_next_z(pis, mus, sigmas)

        term = terminal.cpu().numpy()

        # # gen depth vals 
        # ones_0 = []
        # ones_ = []
        # zeros_0 = []
        # zeros_ = []

        for i, batch in enumerate(z_pred):
            pred_img = vae.forward_decode(batch)
            for j, img in enumerate(pred_img): 
                img = np.array(img.detach().cpu())
             
                # print(terminal.cpu().numpy()[i,j])
                # print('zeros ', np.count_nonzero(img == 0))
                # print('low values', np.count_nonzero((img < 0.3) & (img > 0)))

                # if term[i,j] == 0: 
                #     zeros_0.append(np.count_nonzero(img == 0))
                #     zeros_.append(np.count_nonzero((img > 0) & (img < 0.6)))
                # else: 
                #     ones_0.append(np.count_nonzero(img == 0))
                #     ones_.append(np.count_nonzero((img > 0) & (img < 0.6)))

                low_counts = np.count_nonzero((img < 0.6) & (img > 0))
                    
                if (np.count_nonzero(img == 0) > 3856) or  (low_counts > 100):
                    ds[i,j] = 1
                    
                else:
                    ds[i,j] = 0

        # ones_0 = np.array(ones_0)
        # ones_ = np.array(ones_)
        # zeros_0 = np.array(zeros_0)
        # zeros_ = np.array(zeros_)
        # print("term ones 0 counts", ones_0.min(), ones_0.max(), "  min counts", ones_.min(), ones_.max())
        # print("term zeros 0 counts", zeros_0.min(), zeros_0.max(), "  min counts", zeros_.min(), zeros_.max())



        
        p, r, a, _t = evaluate.compare_ds(ds, term, soft=soft_pra)

        accuracies.append(a)
        if p is not None: 
            precisions.append(p)
        if r is not None:
            recalls.append(r)
            
    
        terminal_ones += np.sum(term)


        optimizer.zero_grad()
        total_loss = get_total_loss(mdrnn, pis, sigmas, mus, rs, ds, z_next, terminal, reward)
        total_loss.backward()
        optimizer.step()



        disp_loss += float(total_loss)
        

        optimizer.step()

        pbar.update(batch_size)


    avg_loss = disp_loss/len(train_loader)
    avg_dloss = done_loss/len(train_loader)
    

    pbar.close()
    avg_precision = sum(precisions)/len(precisions)
    avg_recall = sum(recalls)/len(recalls)
    avg_accuracy = sum(accuracies)/len(accuracies)

    print('Epoch {}  Avg loss: {}  D loss: {} '.format(epoch, avg_loss, avg_dloss)) # divide by num total frames
    print('precision: ', avg_precision, " recall: ", avg_recall, " accuracy: ", avg_accuracy, 'total ones: ', terminal_ones)

    return total_loss, avg_dloss, avg_precision, avg_recall, avg_accuracy


def test(epoch=0):
    mdrnn.eval()
    print(">>>evaluating ..")

    test_loss = 0
    tot_dloss = 0
    
    li = 0
    precisions = []
    accuracies = []
    recalls = []
    terminal_ones=0


    if training:
        loader = val_loader
    else:
        loader = test_loader

    pbar = tqdm(total=len(loader.dataset), desc="Epoch {}".format(epoch))
    batchi = 0
    
    for data in loader:
        batchi += 1
        with torch.no_grad():
            obs, actions, reward, terminal, next_obs = [arr.to(device) for arr in data]
        
            term = terminal.cpu().numpy()
            del data

            li+=1

            terminal_ones += np.sum(term)
            z, z_next = encode_obs(obs, next_obs)

            if len(z.shape) != len(actions.shape): 
                actions = actions.unsqueeze(-1)
            inputs = torch.cat([z, actions], dim=-1)

            pis, sigmas, mus, rs = mdrnn.forward(inputs)

            ds = np.empty_like(rs.detach().cpu())
            z_pred = mdrnn.get_next_z(pis, mus, sigmas)

            for i, batch in enumerate(z_pred):
                pred_img = vae.forward_decode(batch)
                for j, img in enumerate(pred_img): 
                    img = np.array(img.detach().cpu())
                    img = img.reshape((64,64))
                    low_counts = np.count_nonzero((img < 0.6) & (img > 0))

                    if (np.count_nonzero(img == 0) > 4000) or  (low_counts > 200):
                        ds[i,j] = 1
                    else:
                        ds[i,j] = 0

            if examine_mispredicted_frames:
                z_pred = mdrnn.get_next_z(pis, mus, sigmas)
                for i,seq in enumerate(z_pred):
                    pred_imgs = vae.forward_decode(seq)
                    for j, img in enumerate(pred_imgs):
                        if float(ds.data.cpu().numpy()[i,j] > 0.5) != terminal.data.cpu().numpy()[i,j]: 
                            img = toim(img.detach().cpu())
                            realimg = toim(obs.detach().cpu()[i,j])
                            realimg = realimg.resize((320,240), Image.ANTIALIAS)
                            img = img.resize((320,240), Image.ANTIALIAS)
                            img.save('debug/' + str(batchi) + 'batch' + str(i) + '_' + str(j) + 'pred' + str(int(ds.data.cpu().numpy()[i,j] > 0.5)) +  '_' + str(terminal.data.cpu().numpy()[i,j]) + '.jpg')
                            realimg.save('debug/real/' + str(batchi) + 'batch' + str(i) + '_' + str(j) + 'pred' + str(int(ds.data.cpu().numpy()[i,j] > 0.5)) +  '_' + str(terminal.data.cpu().numpy()[i,j]) + '.jpg')




            p, r, a, _t = evaluate.compare_ds(ds, term, soft=soft_pra)

            accuracies.append(a)
            if p is not None: 
                precisions.append(p)
            if r is not None:
                recalls.append(r)


            total_loss = get_total_loss(mdrnn, pis, sigmas, mus, rs, ds, z_next, terminal, reward)

            test_loss += float(total_loss)


            pbar.update(batch_size)


    avg_loss = test_loss/len(val_loader)
    avg_dloss = tot_dloss/len(val_loader)
    avg_precision = sum(precisions)/len(precisions)
    avg_recall = sum(recalls)/len(recalls)
    avg_accuracy = sum(accuracies)/len(accuracies)



    print('Test loss: ', avg_loss, 'Dloss: ', avg_dloss)
    print('precision: ', avg_precision, " recall: ", avg_recall, " accuracy: ", avg_accuracy, 'total ones: ', terminal_ones)

    pbar.close()

    return avg_loss, avg_precision, avg_recall, avg_accuracy


        





if __name__ == "__main__":
    train_losses = []
    val_losses = []
    bestloss = 1e+10

    bestepoch = 0
    no_improve = 0
    patience = 50
    start = 1 

    trte_precision = np.zeros((epochs, 2))
    trte_accuracy = np.zeros((epochs, 2))
    trte_recall = np.zeros((epochs, 2))

    if use_saved_mdnrnn:
        start = ckpt['epoch']
        bestloss = ckpt['testloss']
        if training:
            print("restart training from epoch ", start)

    if training: 
        for epoch in range(start, epochs+1):
            total_loss, trainloss, tp, tr, ta = train(epoch)
            
            trte_precision[epoch-1, 0] = tp ; trte_accuracy[epoch-1, 0] = ta; trte_recall[epoch-1, 0] = tr
            train_losses.append(trainloss)

            testloss, p, r, a = test(epoch=epoch)
            trte_precision[epoch-1, 1] = p ; trte_accuracy[epoch-1, 1] = a; trte_recall[epoch-1, 1] = r
            val_losses.append(testloss)


            
            scheduler.step(testloss)
            

            diff = bestloss - val_losses[epoch-start]
       
            if diff >= 1e-6:
                print('val d_loss decreased by {}, saving model '.format(diff))
                if not os.path.exists(save_path):
                    os.mkdir(save_path)
                path = save_path + model_save_name
                torch.save({
                'epoch': epoch,
                'model_state_dict': mdrnn.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler': scheduler.state_dict(),
                'loss': total_loss,
                'testloss': testloss
                }, path)
                bestloss = testloss

            else: 
                no_improve+=1
                if no_improve > patience:
                    print("Patient for {} epochs. Early stopping at epoch {}".format(patience,epoch))
                    trte_accuracy = trte_accuracy[:epoch, :]
                    trte_precision = trte_precision[:epoch, :]
                    trte_recall = trte_recall[:epoch, :]
                    break

        losses = np.column_stack((train_losses, val_losses))
        np.savetxt(str(epoch) + tagname + '_mdrnnloss.dat', losses)
        np.savetxt(tagname + '_prec.dat', trte_precision); np.savetxt(tagname + '_rec.dat', trte_recall)
        np.savetxt(tagname + '_acc.dat', trte_accuracy)

        
    else: 
        testloss = test()
        result = np.array(testloss)
        pra = result[1:]
        np.savetxt("testpra_" + tagname + "_mdrnn.dat", pra)