#!/usr/bin/env python
# coding: utf-8
import torch
from torch import nn, optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split
from torchvision.utils import save_image
from torchvision import transforms, datasets
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import argparse
import os
from tqdm import tqdm

from utils.data_loader import CustomDataset
from models.vae import VAE



parser = argparse.ArgumentParser(description='Train or test VAE model.')
parser.add_argument('--train', '-t', action='store_true',
                    help='Goes into training mode, otherwise tests model.')

parser.add_argument('--epochs', '-e', type=int, default=50, metavar='int', dest='epochs',
                    help='Train: Set the number of epochs to train.')

parser.add_argument('--save_model', '-sm', type=int, default=100, metavar='int x', dest='save_model',
                    help='Train: Saves model checkpoint every x epochs.')

parser.add_argument('--model_file', '-mf', type=str, default='saved/model_ckpts/bestvae.pt', metavar='FILE', dest='model_path',
                    help='Test: Specify the path to saved model for testing. ')

parser.add_argument('--batchsize', '-bs', type=int, default=32, metavar='int', dest='batchsize',
                    help='Optional: set the batch size.') 

parser.add_argument('--latentdim', '-z', type=int, default=32, metavar='int', dest='latentdim',
                    help='Optional: set the latent dimension size of z for model.')

parser.add_argument('--learning_rate', '-lr', type=float, default=0.001, metavar='float', dest='lr',
                    help='Optional: set the learning rate for training.')

parser.add_argument('--save_imgs', '-si', type=int, default=None, metavar='int x', dest='save_imgs',
                    help='Optional: Saves images every x epochs. If testing, enter any number to save generated images. ')


arg = parser.parse_args()

# config
save_path = 'saved/model_ckpts/' # folder to save checkpoints
img_save_path = 'saved/VaeImages/'

save_input=False

use_saved_model = False

device = torch.device( "cuda" if torch.cuda.is_available() else "cpu")


dataset = CustomDataset('datasets/data_depth_sigmoid/')
lengths = [int(len(dataset)*0.8), int(len(dataset)*0.2)]
# add one to test set if rounding down makes split not equal to total
if sum(lengths) != len(dataset):
    lengths[1] += 1


dataset_train, dataset_val = random_split(dataset, lengths)


train_loader = DataLoader(
    dataset_train, batch_size=arg.batchsize, shuffle=True, num_workers=0, drop_last = True)
val_loader = DataLoader(
    dataset_val, batch_size=arg.batchsize, shuffle=True, num_workers=0, drop_last = True)


test_dataset = CustomDataset('datasets/data_depth_sigmoid/')
test_loader = DataLoader(test_dataset, batch_size=arg.batchsize, num_workers=0, shuffle=True, drop_last=True)

# make instance of vae
vae = VAE(latent_dim=arg.latentdim, kl_tolerance=0.5).to(device)
optimizer = optim.Adam(vae.parameters(), lr=arg.lr)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5, patience=1, verbose=True)
toim = transforms.ToPILImage()

if use_saved_model:
    model_path = 'saved/model_ckpts/twm_vaedepth_relu.pt' # set model path
    ckpt= torch.load(model_path)
    vae.load_state_dict(ckpt['model_state_dict'])
    optimizer.load_state_dict(ckpt['optimizer_state_dict'])
    scheduler.load_state_dict(ckpt['scheduler'])

    print("Loaded VAE at epoch {} with test loss {}".format(ckpt['epoch'], ckpt['testloss']))

params = sum(p.numel() for p in vae.parameters() if p.requires_grad)
print('model trainable params: ', params)


def train_vae(epoch):
    vae.train()
    print(">>training..")

    total_loss = 0
    # out_imgs = []
   
    pbar = tqdm(total=len(train_loader.dataset), desc="Epoch {}".format(epoch))

      
    # real_imgs = []
    for databatch in train_loader:
        databatch = databatch.to(device)
        optimizer.zero_grad()

        mu, logvar = vae.forward_get_params(databatch)
        z = vae.forward_encode(databatch)
        data_reconstructed = vae.forward_decode(z)
        loss = vae.get_loss(data_reconstructed, databatch, mu, logvar)

        # save images
        # out_imgs.append(data_reconstructed)
        # real_imgs.append(databatch)

        loss.backward()

        total_loss += loss.item()

        optimizer.step()

        pbar.update(arg.batchsize)
       
    
    avg_img_loss = total_loss / len(train_loader.dataset)

    print('Epoch: {}   Loss: {}'.format(epoch, avg_img_loss)) 

    pbar.close()

    
    if arg.save_imgs is not None:
        if not os.path.exists(img_save_path):
            os.mkdir(img_save_path)
        if epoch%arg.save_imgs == 0:
            for i, image in enumerate(data_reconstructed):
                img = toim(image.cpu().detach())
                img = img.resize((320,240), Image.ANTIALIAS)
                img.save(img_save_path + 'trainoutput' + str(epoch) + '_' + str(i) + '.jpg')

    # save model 
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    path = save_path + 'vae' + str(epoch) + '.pt'
    if epoch%arg.save_model == 0:
        torch.save({
        'epoch': epoch,
        'model_state_dict': vae.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'loss': loss
        }, path)


    return total_loss, avg_img_loss




def test_vae(epoch=None, model_path = None, saved_model = False):
    # specify path to saved model, whether to save input images, default uses saved model
    print(">>>evaluating..")
    if saved_model: 
        ckpt= torch.load(model_path)
        vae.load_state_dict(ckpt['model_state_dict'])
        optimizer.load_state_dict(ckpt['optimizer_state_dict'])
        epoch = ckpt['epoch']
        loss = ckpt['loss']

    vae.eval()

    total_loss=0

    if arg.train:
        loader = val_loader
    else:
        loader = test_loader

    pbar = tqdm(total=len(val_loader.dataset), desc="Epoch {}".format(epoch))

    with torch.no_grad():
        for databatch in loader:
            databatch = databatch.to(device)

            mu, logvar = vae.forward_get_params(databatch)
            z = vae.forward_encode(databatch)
            data_reconstructed = vae.forward_decode(z)
            # debug
            img = np.array(data_reconstructed.detach().cpu())
            print(img.max(), img.min())

            loss = vae.get_loss(data_reconstructed, databatch, mu, logvar)

            total_loss += loss.item()

            pbar.update(arg.batchsize)


        avg_img_loss = total_loss/ len(val_loader.dataset)
        
        pbar.close()

      
        if arg.save_imgs is not None:
            if not os.path.exists(img_save_path):
                os.mkdir(img_save_path)
            if epoch%arg.save_model == 0:
                for i, image in enumerate(data_reconstructed):
                    img = toim(image.detach().cpu())
                    img = img.resize((320,240), Image.ANTIALIAS)
                    img.save(img_save_path + 'testoutput' + str(epoch) + '_' + str(i) + '.jpg')
        if save_input:
            for i, image in enumerate(databatch):
                img = toim(image)
                img = img.resize((320,240), Image.ANTIALIAS)
                img.save(img_save_path + 'real' + str(epoch) + '_' + str(i) + '.jpg')

    print('Epoch {}, Test Loss: {}'.format(epoch, avg_img_loss))
    return avg_img_loss





if __name__ == "__main__":
    if arg.train:
        train_losses = []
        val_losses = []
        bestloss = 1e+10
        no_improve = 0
        patience = 50

        start = 1
        if use_saved_model:
            start = ckpt['epoch']
            bestloss = ckpt['testloss']

            print("restart training from epoch ", start)

        for epoch in range(start, arg.epochs+1):
            total_loss, trainloss = train_vae(epoch=epoch)
            train_losses.append(trainloss)

            testloss = test_vae(epoch=epoch)
            val_losses.append(testloss)

    
            scheduler.step(testloss)

            # ckpt best & early stopping
            diff = bestloss - val_losses[epoch-start]
     
            if diff > 1e-10:
                
                if not os.path.exists(save_path):
                    os.mkdir(save_path)
                path = save_path + 'twm_vaedepth_sig' + '.pt'
                torch.save({
                'epoch': epoch,
                'model_state_dict': vae.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler': scheduler.state_dict(),
                'loss': total_loss,
                'testloss': testloss
                }, path)
                bestloss = testloss
                print('val loss decreased by {}, saving model '.format(diff))
            else: 
                no_improve+=1
                if no_improve > patience:
                    print("Patient for {} epochs. Early stopping at epoch {}".format(patience,epoch))
                    break

        losses = np.column_stack((train_losses, val_losses))
        np.savetxt(str(epoch) + 'vaeloss.dat', losses)
    else:
        test_vae(model_path=arg.model_path)

