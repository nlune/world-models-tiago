from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader
from torchvision import transforms
from PIL import Image
import os
import numpy as np
import torch
import matplotlib.pyplot as plt
import pandas as pd


class CustomDataset(Dataset):
    def __init__(self, data_root_files): 
        # self.transform = transforms.Compose([#transforms.Grayscale(num_output_channels=3), 
        #                                    transforms.ToTensor()])
        # self.transform = transforms.Lambda(lambda x: np.transpose(x, (0, 3, 1, 2)) / 255)

        print("loading & preparing data")

        self.data = {}

        for i, data_root in enumerate(os.listdir(data_root_files)):
            d = np.load(data_root_files + "/" + data_root)

            for key in d.files:
                if i == 0:
                    self.data[key] = d[key]
                        # print("first go", key, d[key].shape)
                else:
                    self.data[key] = np.append(self.data[key], d[key], axis=0)
                #         # print("later ", key,  self.data[key].shape)

        
        self.total = len(self.data['images'])

        print('transforming images...')

        self.images =self.data['images'] 
        print(self.images.dtype)
        self.dones =self.data['dones'] 
        self.rewards =self.data['rewards'] 
        self.actions =self.data['actions'] 



    def __getitem__(self, seq_index):
        img = self.images[seq_index]

        return img

    def __len__(self): 
        return len(self.images)

    def _data_per_sequence(self, data_length):
        pass

    def _get_data(self, seq_index):
        pass 



class CustomSeqDataset(CustomDataset): 
    def __init__(self,  data_root, seq_len): 
        super().__init__(data_root) 
        self.seq_len = seq_len

    def __getitem__(self, seq_index):
         
        obs_data = self.images[seq_index:seq_index + self.seq_len + 1]
        # obs_data = self.transform(obs_data.astype(np.float32))
        obs, next_obs = obs_data[:-1], obs_data[1:]
        action = self.actions [seq_index+1:seq_index + self.seq_len + 1]
        action = action.astype(np.float32)
        reward = self.rewards[seq_index+1:seq_index + self.seq_len + 1].astype(np.float32)
        done = self.dones[seq_index+1:seq_index + self.seq_len + 1].astype(np.float32)

        return obs, action, reward, done, next_obs


    def __len__(self):

        return len(self.dones) - self.seq_len 



# import scipy.misc

# test = CustomSeqDataset('datasets/tiago_depth', 32)
# loader = DataLoader(test, batch_size=16, shuffle=False, num_workers=0, drop_last=True)

# for i,batch in enumerate(loader):
    
#     obs, actions, reward, terminal, next_obs = batch

#     print(obs.shape)

#     print( terminal.data.cpu().numpy()[0,0], end =" ")
    
#     scipy.misc.imsave('debug/outfile' + str(i) + '.jpg', obs[0,0])

    # if i == 300:
    #     break

    # print(obs.shape)


# test = CustomDataset('data')

# loader = DataLoader(test, batch_size=16, shuffle=True, num_workers=0)

# for batch in loader:
#     print(batch.shape)
