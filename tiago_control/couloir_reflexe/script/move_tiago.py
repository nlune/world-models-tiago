#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist


def move(linear_x = 0, angular_z = 0.36):
    msg = Twist()
    pub_cmdv = rospy.Publisher('/mobile_base_controller/cmd_vel', Twist, queue_size=30)
    rospy.init_node('straight_line')
    rate = rospy.Rate(10) # 10hz

    while not rospy.is_shutdown():
        msg.linear.x = linear_x
        msg.angular.z = angular_z
        pub_cmdv.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    # move(linear_x = -0.2, angular_z=0.0)

    move(angular_z=-0.36)
#     move()