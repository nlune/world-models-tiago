#!/usr/bin/env python
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torchvision import transforms
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image
from tqdm import tqdm
import gc


#todo research optimal num Gaus, lstm hidden, etc
# todo extend lstm and. 
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
class MDN_RNN_Base(nn.Module):
    def __init__(self, action_size, z_dim, temp = 1.15, n_layers = 1, lstm_hidden = 256, h_size=256, n_gauss=5):
        super(MDN_RNN_Base, self).__init__()
        self.n_gauss = n_gauss
        self.z_dim = z_dim
        self.lstm_hidden = lstm_hidden
        self.action_size = action_size
        self.n_layers = n_layers
        self.temp = temp


        self.pi_out = nn.Linear(lstm_hidden, n_gauss)
        self.sig_out = nn.Linear(lstm_hidden, n_gauss*z_dim)
        self.mu_out = nn.Linear(lstm_hidden, n_gauss*z_dim)


        self.predict_out = nn.Linear(lstm_hidden, 2) 



class MDN_RNN(MDN_RNN_Base):
    def __init__(self, batch_size, seq_len, action_size, z_dim, temp = 1.15, n_layers = 1, lstm_hidden = 256, h_size=256, n_gauss=5):
        super(MDN_RNN, self).__init__(action_size, z_dim, temp = 1.15, n_layers = 1, lstm_hidden = 256, h_size=256, n_gauss=5)

        self.seq_len = seq_len
        self.batch_size = batch_size

        self.rnn = nn.LSTM(z_dim + self.action_size, lstm_hidden, batch_first=True) 

    def forward(self, inputs):
        outs, _hidden = self.rnn(inputs)

        pis = self.pi_out(outs)
        pis = pis.view(self.batch_size, self.seq_len, self.n_gauss)
        pis = F.softmax(pis, dim=-1)
        pis = pis/self.temp 

        sigmas = self.sig_out(outs)
        sigmas = sigmas.view(self.batch_size, self.seq_len, self.n_gauss, self.z_dim)
        sigmas = torch.exp(sigmas) * self.temp 

        mus = self.mu_out(outs)
        mus = mus.view(self.batch_size, self.seq_len, self.n_gauss, self.z_dim)

        predictions = self.predict_out(outs)
        rs = predictions[:,:,0]

        return pis, sigmas, mus, rs#, ds

    def get_next_z(self, pi, mu, sigma):
        # samples predicted latent vector
        eps = torch.rand(*mu.shape).to(device)

        z_pred = eps*sigma*self.temp + mu
        z_pred = torch.sum(pi[:,:,:,None] * z_pred, 2)

        return z_pred

    def get_loss(self, target, pi, mu, sigma, reduce=True):
        # pi, mu, and sigma are outputs
        y = torch.unsqueeze(target, 2) # [BS, SEQ_LEN, 1, Z_DIM]
        distr = distributions.Normal(loc=mu, scale=sigma)
        prob = torch.exp(distr.log_prob(y)) # [BS, SEQ_LEN, N_GAUS, Z_DIM]
        loss = torch.sum(pi[:,:,:,None] * prob, 2) # [BS, SEQ_LEN, Z_DIM]
        loss = -torch.log(loss + 1e-40)
        if reduce:
            return loss.mean()

        return loss

class MDN_RNN_Cell(MDN_RNN_Base):
    def __init__(self, action_size, z_dim, temp = 1.15, n_layers = 1, lstm_hidden = 256,  h_size=256, n_gauss=5):
        super(MDN_RNN_Cell, self).__init__(action_size, z_dim)
        self.rnn = nn.LSTMCell(z_dim + self.action_size, lstm_hidden) 

    def forward(self, input, hidden):

        nexthidden = self.rnn(input, hidden)
        outs = nexthidden[0]

        pis = self.pi_out(outs)
        pis = pis.view(-1, self.n_gauss)
        pis = F.softmax(pis, dim=-1)
        pis = pis/self.temp


        sigmas = self.sig_out(outs)
        sigmas = sigmas.view(-1, self.n_gauss, self.z_dim)
        sigmas = torch.exp(sigmas) *self.temp

        mus = self.mu_out(outs)
        mus = mus.view(-1, self.n_gauss, self.z_dim)

        predictions = self.predict_out(outs)
        rs = predictions[:,0]

        return pis, sigmas, mus, rs, nexthidden

    def get_next_z(self, pi, mu, sigma):
        # samples predicted latent vector
        eps = torch.rand(*mu.shape).to(device)

        z_pred = eps*sigma + mu # [ 1, 5, 32]
        z_pred = torch.sum(pi[:,:,None] * z_pred, 1)

        return z_pred

    def get_loss(self, target, pi, mu, sigma, reduce=True):
        y = torch.unsqueeze(target, 1) 
        distr = distributions.Normal(loc=mu, scale=sigma)
        loss = torch.exp(distr.log_prob(y)) 
        loss = torch.sum(pi[:,:,None] * loss, 1) 
        loss = -torch.log(loss + 1e-40) 
        if reduce:
            return loss.mean()

        return loss

    



