#!/usr/bin/env python
# coding: utf-8
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image as Img
from tqdm import tqdm
import argparse
import time

import pygame 
import rospy
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
import ros_numpy
from std_msgs.msg import Bool
from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud2
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Twist
from message_filters import ApproximateTimeSynchronizer, Subscriber
from cv_bridge import CvBridge, CvBridgeError
import cv2

from vae import VAE
from mdnrnn import MDN_RNN_Cell


save_imgs = False

pygame.init()
pygame.joystick.init()
joystick_count = pygame.joystick.get_count()
for i in range(joystick_count):
    joystick = pygame.joystick.Joystick(i)
    joystick.init()
    buttons = joystick.get_numbuttons()
    print("Joystick {}, # of buttons: {}".format(i, buttons))
name = joystick.get_name()
print("Joystick name: {}".format(name) )


bridge = CvBridge()

device = torch.device("cpu")

global sync_cmdvel; sync_cmdvel = None
global cloud_z; cloud_z = None


def callback(cmd_vel, pt_cloud):
    global sync_cmdvel
    sync_cmdvel = [cmd_vel.twist.linear.x, cmd_vel.twist.linear.y, cmd_vel.twist.linear.z, cmd_vel.twist.angular.x, cmd_vel.twist.angular.y, cmd_vel.twist.angular.z]
    sync_cmdvel = np.array(sync_cmdvel)

    global cloud_z
    cloud = ros_numpy.point_cloud2.pointcloud2_to_array(pt_cloud)
    cloud_z = np.nan_to_num(cloud['z'])


cmd_vel_sub = Subscriber("/mobile_base_controller/cmd_vel_out", TwistStamped) 
depth_sub = Subscriber("/xtion/depth/points", PointCloud2)

ats = ApproximateTimeSynchronizer([cmd_vel_sub, depth_sub], queue_size=10, slop=0.1) #slop is error btwn times of the msgs
ats.registerCallback(callback)

# publish done and cmd_vel
pub_done = rospy.Publisher('done', Bool, queue_size = 10)
pub_cmdv = rospy.Publisher('/mobile_base_controller/cmd_vel', Twist, queue_size=10)

rospy.init_node('reflex')
rate = rospy.Rate(10) # 10hz

z_dim = 32
done = False

# instantiate models
# VAE
vae = VAE().to(device)  
ckpt= torch.load('/home/lune/catkin_ws/src/couloir_reflexe/script/model_ckpts/twm_vaedepth_relu.pt', map_location='cpu')
vae.load_state_dict(ckpt['model_state_dict'])
print("Loaded VAE at epoch {} with test loss {} ".format(ckpt['epoch'], ckpt['testloss']))

# MDRNN
action_size = 6
mdrnncell= MDN_RNN_Cell(action_size = action_size, z_dim = z_dim).to(device)
ckpt= torch.load('/home/lune/catkin_ws/src/couloir_reflexe/script/model_ckpts/twm_mdrnn_depthrelu.pt', map_location='cpu')
state_dict = {k.strip('_l0'): v for k, v in ckpt['model_state_dict'].items()} # remove layer0 from key
mdrnncell.load_state_dict(state_dict)
mdrnncell.eval()

print("Loaded MDN_RNN")

# initial hidden & cell states
h = torch.zeros(1, 256, device=device)
c = torch.zeros(1, 256, device=device)


msg = Twist()
frame = 0

while not rospy.is_shutdown():
    pub_done.publish(done)
    reflex = False
    done = False
    # if done:
    #     break

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.JOYHATMOTION:
            if event.value == (0,0):
               msg.linear.x = 0.0 # "stop"
               msg.angular.z = 0.0
            elif event.value == (0,1):
                msg.linear.x = 0.3 # "forward"
                msg.angular.z = 0.0
            elif event.value == (1,1):
                msg.linear.x = 0.3
                msg.angular.z = -0.5# forward right
            elif event.value == (-1, 1):
                msg.linear.x = 0.3
                msg.angular.z = 0.5 # forward left
            elif event.value == (-1, -1):
                msg.linear.x = -0.3
                msg.angular.z = -0.5 # backward left
            elif event.value == (1. -1):
                msg.linear.x = -0.3
                msg.angular.z = 0.5 # backward right
            elif event.value == (0, -1):
                msg.linear.x = -0.2 # "backward"
                msg.angular.z = 0.0
            elif event.value == (-1, 0):
                msg.angular.z = 0.5 #"left"
            elif event.value == (1, 0):
                msg.angular.z = -0.5

    
        
    if sync_cmdvel is not None and cloud_z is not None: 
        frame += 1
        arr = cv2.resize(cloud_z, (64,64))
        # save img
        if save_imgs:
            m = np.amax(arr); print('arr max ', m)
            norm_arr = arr / 8
            img = Img.fromarray(np.uint8(norm_arr*255), 'L')
            img = img.resize((640, 480), Img.ANTIALIAS)
            img.save('/home/lune/catkin_ws/src/couloir_reflexe/script/images2/depth' + str(frame) + '.png')
            print('saved frame', frame)
            

        arr = np.expand_dims(np.expand_dims(arr, axis=0), 0)
        arr = torch.from_numpy(arr)
        z_next = vae.forward_encode(arr)

        action = np.expand_dims(sync_cmdvel, 0)
        action = torch.from_numpy(action)
        
        action = action.type(torch.FloatTensor)

        for i in range(5): 
            print('simulating....')
            
            inp = torch.cat([z_next, action], dim=-1) 

            pi, sigma, mu, r, (h, c) = mdrnncell.forward(inp, (h, c))

            z_next = mdrnncell.get_next_z(pi, mu, sigma)

            predictedimg = vae.forward_decode(z_next) 
            
            # get predictions based on depth img
            img_arr = predictedimg.detach().numpy()
            img_arr = np.reshape(img_arr, (64, 64))

            # save img
            if save_imgs:
                m = np.amax(img_arr); print('arr max ', m)
                norm_arr = img_arr / 8
                img = Img.fromarray(np.uint8(norm_arr*255), 'L')
                img = img.resize((640, 480), Img.ANTIALIAS)
                img.save('/home/lune/catkin_ws/src/couloir_reflexe/script/images_predicted2/depth' + str(frame) + '_sim' + str(i + 1) + '.png')
                print('saved frame', frame)
                      

            
            low_counts = np.count_nonzero((img_arr < 0.30) & (img_arr > 0))
            nans_count = np.count_nonzero(img_arr == 0) 


            print('num low counts ', low_counts)
            print('num nans ', nans_count)


            if (nans_count > 3800) or (low_counts > 300):
                reflex = True
                print('low detected step ', i)
                left_nans = np.count_nonzero(img_arr[:,:25] == 0.0)
                right_nans = np.count_nonzero(img_arr[:,40:] == 0.0)
                centre_nans = np.count_nonzero(img_arr[25:40] == 0.0)
                print('nans left right centre ', left_nans, right_nans, centre_nans)
                low_counts_left = np.count_nonzero((img_arr[:,:24] < 0.30) & (img_arr[:,:24] > 0))
                low_counts_right = np.count_nonzero((img_arr[:,40:] < 0.30) & (img_arr[:, 40:] > 0))
                low_counts_mid = np.count_nonzero((img_arr[:,25:40] < 0.30) & (img_arr[:,25:40] > 0)) 
                print('low left right centre ', low_counts_left, low_counts_right, low_counts_mid)

                low_diff = low_counts_left - low_counts_right
                low_diff_nan = left_nans - right_nans
            

                if msg.linear.x > 0.15:
                    msg.linear.x = 0.15

                # something on side
                if low_diff > 60 or low_diff_nan > 600: 
                    msg.angular.z = -0.6
                    print('turned right at step ', i)
                if low_diff < -60 or low_diff_nan < -600:
                    msg.angular.z = 0.6
                    print('turned left at step ', i)
                
                if i<=1:
                    # predict collision
                    reflex = True
                    print('collision detected at step ', i)

                    low_diff = low_counts_left - low_counts_right
                    if low_diff > 60 or low_diff_nan > 600: 
                        msg.angular.z = -0.8
                        print('turned right at step ', i)
                    if low_diff < -60 or low_diff_nan > 600:
                        msg.angular.z = 0.8
                        print('turned left at step ', i)
    
                    if msg.linear.x > 0:
                        msg.linear.x = 0.0
                        done = True 
                        # break

                pub_cmdv.publish(msg)


            else:
                if reflex:
                    msg.angular.z = 0.0
                pub_cmdv.publish(msg)
                print('not done. still going... ')

    rate.sleep()




       
        
