#!/usr/bin/env python
# coding: utf-8
# run navigate_tiago_reflex or tiago_reflex_joystick before starting this program
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image
from tqdm import tqdm
import argparse
import time

import rospy
import ros_numpy
from std_msgs.msg import Bool
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
from sensor_msgs.msg import PointCloud2
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Twist
from message_filters import ApproximateTimeSynchronizer, Subscriber
from cv_bridge import CvBridge, CvBridgeError
import cv2

from vae import VAE
from mdnrnn import MDN_RNN_Cell, MDN_RNN

# time.sleep(5)
train_vae = True 

bridge = CvBridge()

device = torch.device("cpu")

global sync_cmdvel; sync_cmdvel = None
global cloud_z; cloud_z = None
global done; done = Bool()


def callback(cmd_vel, pt_cloud):
    global sync_cmdvel
    sync_cmdvel = [cmd_vel.twist.linear.x, cmd_vel.twist.linear.y, cmd_vel.twist.linear.z, cmd_vel.twist.angular.x, cmd_vel.twist.angular.y, cmd_vel.twist.angular.z]
    sync_cmdvel = np.array(sync_cmdvel)

    global cloud_z
    cloud = ros_numpy.point_cloud2.pointcloud2_to_array(pt_cloud)
    cloud_z = np.nan_to_num(cloud['z'])


def cb_done(d):
    global done
    done = d.data


cmd_vel_sub = Subscriber("/mobile_base_controller/cmd_vel_out", TwistStamped) 
depth_sub = Subscriber("/xtion/depth/points", PointCloud2)
done_sub = rospy.Subscriber("/done", Bool, cb_done)

ats = ApproximateTimeSynchronizer([cmd_vel_sub, depth_sub], queue_size=40, slop=0.1) #slop is error btwn times of the msgs
ats.registerCallback(callback)
pub_dpth = rospy.Publisher('depth_arr', numpy_msg(Floats), queue_size= 40)

rospy.init_node('realtime_training', anonymous=True)
rate = rospy.Rate(10) # 10hz 

# config
z_dim = 32
action_size = 6
learning_rate = 0.0007

# instantiate models
# VAE
vae = VAE().to(device)  
ckpt= torch.load('/home/lune/catkin_ws/src/couloir_reflexe/script/model_ckpts/twm_vaedepth_relu.pt', map_location='cpu')
vae.load_state_dict(ckpt['model_state_dict'])
if train_vae:
    vae.train()
print("Loaded VAE at epoch {} with test loss {} ".format(ckpt['epoch'], ckpt['testloss']))

# MDRNN for training, seqlen 32
mdrnn = MDN_RNN(1, 32, action_size = action_size, z_dim = z_dim).to(device)
ckpt= torch.load('/home/lune/catkin_ws/src/couloir_reflexe/script/model_ckpts/twm_mdrnn_depthrelu.pt', map_location='cpu')
mdrnn.load_state_dict(ckpt['model_state_dict'])
mdrnn.train()
print("Loaded MDN_RNN")

# optimize
m_optimizer = optim.Adam(mdrnn.parameters(), lr=learning_rate)
v_optimizer = optim.Adam(vae.parameters(), lr=learning_rate)

depth_seq = []
action_seq = []

while not rospy.is_shutdown():
    if sync_cmdvel is not None and cloud_z is not None: 
        arr = cv2.resize(cloud_z, (64,64))
        depth_seq.append(arr)
        action_seq.append(sync_cmdvel)

        if len(action_seq) == 33: 
            print(len(depth_seq))
            depth_curr = depth_seq[:32]
            depth_next = depth_seq[1:]
            
            action_curr = action_seq[:32]
            action_next = action_seq[1:]
            print(len(depth_curr), len(depth_next))

            depth_tensor_original = torch.from_numpy(np.array(depth_curr))


            depth_tensor_original = depth_tensor_original.unsqueeze(1)
        
            depth_tensor = vae.forward_encode(depth_tensor_original)
            print('depth ', depth_tensor.shape) 
            
            if train_vae:
                mu, logvar = vae.forward_get_params(depth_tensor_original)
                reconstructed = vae.forward_decode(depth_tensor)
                v_optimizer.zero_grad()
                vloss = vae.get_loss(reconstructed, depth_tensor_original, mu, logvar)
                vloss.backward(retain_graph=True)
                v_optimizer.step()
                print('took a step vae, loss ', vloss.item())

            depth_tensor = depth_tensor.unsqueeze(0)
            print('depth after ', depth_tensor.shape) # [1, 32, 32]

            depthnext_tensor = torch.from_numpy(np.array(depth_next))
            depthnext_tensor = depthnext_tensor.unsqueeze(1)
            depthnext_tensor = vae.forward_encode(depthnext_tensor)
            depthnext_tensor = depthnext_tensor.unsqueeze(0)

            action_tensor = torch.from_numpy(np.array(action_curr))
            action_tensor = action_tensor.unsqueeze(0)
            action_tensor = action_tensor.type(torch.FloatTensor)
            print(action_tensor.shape) # [1, 32, 6]


            inp = torch.cat([depth_tensor, action_tensor], dim=-1) 

            pi, sigma, mu, r = mdrnn.forward(inp)

            m_optimizer.zero_grad()
            loss = mdrnn.get_loss(depthnext_tensor, pi, mu, sigma)
            loss.backward()
            m_optimizer.step()
            print('took a step mdrnn, loss ', loss.item())

            depth_seq = []
            action_seq = []


        if done: 
            path = '/home/lune/catkin_ws/src/couloir_reflexe/script/model_ckpts/twm_mdrnn_depthrelu.pt'
            torch.save({
            'model_state_dict': mdrnn.state_dict(),
            'm_optimizer_state_dict': m_optimizer.state_dict(),
            }, path)
            print('saved model')
            break


    rate.sleep()


       
        
