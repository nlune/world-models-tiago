#!/usr/bin/env python
import pygame 

import rospy
from geometry_msgs.msg import Twist


pygame.init()
pygame.joystick.init()
joystick_count = pygame.joystick.get_count()

msg = Twist()
pub_cmdv = rospy.Publisher('/mobile_base_controller/cmd_vel', Twist, queue_size=10)
rospy.init_node('joystick')
rate = rospy.Rate(10)

for i in range(joystick_count):
    joystick = pygame.joystick.Joystick(i)
    joystick.init()
    buttons = joystick.get_numbuttons()
    print("Joystick {}, # of buttons: {}".format(i, buttons))
name = joystick.get_name()
print("Joystick name: {}".format(name) )


while not rospy.is_shutdown():
    # joystick 
    pub_cmdv.publish(msg)
    for event in pygame.event.get():
        print(event.type, event.value)
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.JOYHATMOTION:
            if event.value == (0,0):
               msg.linear.x = 0.0 # "stop"
               msg.angular.z = 0.0
            elif event.value == (0,1):
                msg.linear.x = 0.3 # "forward"
            elif event.value == (1,1):
                msg.linear.x = 0.3
                msg.angular.z = -0.5# forward right
            elif event.value == (-1, 1):
                msg.linear.x = 0.3
                msg.angular.z = 0.5 # forward left
            elif event.value == (-1, -1):
                msg.linear.x = -0.3
                msg.angular.z = -0.5 # backward left
            elif event.value == (1. -1):
                msg.linear.x = -0.3
                msg.angular.z = 0.5 # backward right
            elif event.value == (0, -1):
                msg.linear.x = -0.2 # "backward"
            elif event.value == (-1, 0):
                msg.angular.z = 0.3 #"left"
            elif event.value == (1, 0):
                msg.angular.z = -0.3 # "right"
