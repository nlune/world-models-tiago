# World Models
Adjusted implementation of [World Models](https://arxiv.org/abs/1803.10122), by Ha & Schmidhuber, using Pytorch and applied to depth images from Tiago robot. 


## Data Gathering using Tiago robot
- create a package for catkin_ws with script `data_processing/ros_script/timesync.py`
- record rosbag with topics of interest, eg. `rosbag record -O name.bag /synced_cmdvel /synced_dpth` 
- run `python extract_rosbag_depth.py name.bag` to extract data to folders 
- run `create_data_depth.py` to get the .npz file for training (change file paths)
- move .npz file to appropriate folder under `/datasets`


## Variational Auto-Encoder

- To look at the different options, run:    
`./train_test_vae.py --help `

- To train the model, run `./train_test_vae.py --train`  

    Eg. `./train_test_vae.py -t --epochs 20 --save_model 20`
    
    Saved models can be found in folder 'model_ckpts', and saved images can be found in folder 'imgs'. 

- To test the model after training, run `./train_test_vae.py` along with folder path to saved model, etc. 

    Eg. `./train_test_vae.py  --path_test_data data_folder  --model_file model_ckpts/vae20.pt`


## MDN-RNN 

## Using Model to Predict & React in Real-time
- using Python 2.7 and ROS Kinetic
- add the couloir_reflexe folder to catkin_ws



