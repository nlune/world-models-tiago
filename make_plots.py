#!/usr/bin/env python
# coding: utf-8
# plotting and stat functions
import matplotlib.pyplot as plt
import numpy as np
import collections

def pra_final_plot(avgs, stds):
	# plots avg and std of acc, prec, recall created by avg_n_std_list()
	bb_avgs = [] # base & best
	bb_stds = []

	lege = ["Recall Baseline train", "Recall Baseline test", "Recall Adj5ws5 Model train", "Recall Adj5ws5 Model test"]
	styles = ['b-', 'b--','r-', 'r--', 'g-', 'g--']
	colours = ["blue", "red"]

	f = plt.figure(figsize=(14,8), frameon=True, clear=False)

	for f in avgs: 
		b = np.loadtxt(f)
		bb_avgs.append(b)

	for f in stds: 
		b = np.loadtxt(f)
		bb_stds.append(b)


	x = np.arange(1, len(b) + 1)

	for i, dat in enumerate(bb_avgs): 

		for j in range(2): 
			
			plt.plot(x, dat[:, j], styles[2*i+j], label = lege[2*i+j])

			plt.fill_between(x, dat[:,j] + bb_stds[i][:, j], dat[:,j] - bb_stds[i][:, j], facecolor=colours[i], alpha=0.2)

	plt.title("Recall over Training -- Baseline vs Adjusted Model Comparison", pad=15)
	plt.ylim([0, 1])
	plt.xlabel("Epochs")
	plt.legend()
	plt.show()
	 


def sim_final_plot(avgs, stds, title): 
	# plots avg and std created by avg_n_std_dict() for simulation horizon
	bb_avgs = [] 
	bb_stds = []

	lege = ["Accuracy", "Recall", "Soft Precision (10 step)"]
	styles = ['-', 'r-', 'r--', 'b-', 'b--', 'g-', 'g--']

	f = plt.figure(figsize=(20,4.5), frameon=True, clear=False)
	ax1 = f.add_subplot(131)
	ax2 = f.add_subplot(132)
	ax3 = f.add_subplot(133)

	for f in avgs: 
		b = np.load(f)[()]
		b = {int(k) : v for k,v in b.items()}
		dic = collections.OrderedDict(sorted(b.items()))
		bb_avgs.append(dic)

	for f in stds: 
		b = np.load(f)[()]
		b = {int(k) : v for k,v in b.items()}
		dic = collections.OrderedDict(sorted(b.items()))
		bb_stds.append(dic)

	index = dic.keys()

	p_avg = np.zeros((len(index), 2)) 
	r_avg = np.zeros((len(index), 2)) 
	a_avg = np.zeros((len(index), 2)) 


	p_std = np.zeros((len(index), 2)) 
	r_std = np.zeros((len(index), 2)) 
	a_std = np.zeros((len(index), 2)) 

	

	for i, dic in enumerate(bb_avgs): 
		for j, v in enumerate(dic.values()): 
			p_avg[j][i] = v[0]
			r_avg[j][i] = v[1]
			a_avg[j][i] = v[2]

	for i, dic in enumerate(bb_stds): 
		for j, v in enumerate(dic.values()): 
			p_std[j][i] = v[0]
			r_std[j][i] = v[1]
			a_std[j][i] = v[2]

	for i in range(1): 
		ax1.plot(index, a_avg[:,i], styles[i], label=lege[i])
		ax1.fill_between(index, a_avg[:,i]+ a_std[:,i], a_avg[:,i] - a_std[:,i], facecolor='red', alpha=0.2)

		ax2.plot(index, r_avg[:,i], styles[i], label=lege[i+1])
		ax2.fill_between(index, r_avg[:,i]+ r_std[:,i], r_avg[:,i] - r_std[:,i], facecolor='red', alpha=0.2)

		ax3.plot(index, p_avg[:,i], styles[i], label=lege[i+2])
		ax3.fill_between(index, p_avg[:,i]+ p_std[:,i], p_avg[:,i] - p_std[:,i], facecolor='red', alpha=0.2)



	print(r_avg)
	print(p_avg)
		

	ax1.legend()
	ax1.set_ylim([-0.01 ,1])

	ax2.legend()
	ax2.set_ylim([-0.01, 1.0])

	ax3.legend()
	ax3.set_ylim([-0.01, 1.0])

	ax2.set_title(title, pad=15)
	ax2.set_xlabel("Number of Simulated Steps")

	plt.tight_layout()
	plt.show()





def avg_n_std_list(files):
	# saves avg and std of .dat files (pra over training epochs)
	dats = []
	for f in files:
		dat = np.loadtxt(f)
		dats.append(dat)


	avgs = np.zeros_like(dat)
	stds = np.zeros_like(dat)

	# get avgs
	for f in dats:
		for i in range(len(f)):
			for j in range(len(f[0])):
				avgs[i,j] += f[i,j]

	avgs = avgs / len(dats)

	# get stds
	for f in dats:
		for i in range(len(f)):
			for j in range(len(f[0])):
				stds[i,j] += (f[i,j] - avgs[i,j]) **2


	stds = np.sqrt(stds / (len(dats) - 1))

	print(dats)
	print("avgs \n", avgs, "\n stds \n",  stds)

	np.savetxt("best_rec_avgs.dat", avgs)
	np.savetxt("best_rec_stds.dat", stds)


def avg_n_std_dict(files):
	# saves avg and std of .npy files (pra over simulation horizon)
	dics = []


	for f in files:
		d = np.load(f)[()]
		d = {int(k) : v for k,v in d.items()}
		d = collections.OrderedDict(sorted(d.items()))
		dics.append(d)

	keys = d.keys()
	d_avg = {k: [0,0,0] for k in keys}
	d_std = {k: [0,0,0] for k in keys}

	# get the avgs
	for d in dics:
		for k in keys:

			d_avg[k][0] += d[k][0]
			d_avg[k][1] += d[k][1]
			d_avg[k][2] += d[k][2]



	d_avg = {k: np.asarray(v)/len(dics) for k,v in d_avg.items()}

	# get the stds
	for d in dics:
		for k in keys:
			d_std[k][0] += (d[k][0] - d_avg[k][0])**2
			d_std[k][1] += (d[k][1] - d_avg[k][1])**2
			d_std[k][2] += (d[k][2] - d_avg[k][2])**2


	d_std = {k: np.sqrt(np.asarray(v) / (len(dics) -1)) for k,v in d_std.items() }

	np.save("sim_avg.npy", d_avg)
	np.save("sim_std.npy", d_std)





def plot_dict(filename, file2 = None, title="Done Prediction over Simulated Horizon"):
	# plot for simulation horizon
	dic = np.load(filename)[()]
	dic = {int(k) : v for k,v in dic.items()}
	dic = collections.OrderedDict(sorted(dic.items()))


	index = dic.keys()

	plt.figure(figsize=(14,6), frameon=True, clear=False)


	p = []
	r = []
	a = []
	for k, v in dic.items():
		print(k, v)
		p.append(v[0])
		r.append(v[1])
		a.append(v[2])


	plt.plot(index, p, 'b-', label="Precision")
	plt.plot(index, r, 'r-', label="Recall")
	plt.plot(index, a, 'g-', label="Accuracy")

	if file2:
		style = ['bo', 'ro', 'go']
		dat = np.loadtxt(file2)
		for i, v in enumerate(dat):
			plt.plot(0, v, style[i] )


	# bar plot
	# index = np.arange(len(dic))
	# bar_width = 0.25
	# opacity = 0.8
	# plt.bar(index - bar_width, p , bar_width, alpha=opacity, color ='b', label='Precision')
	# plt.bar(index, r, bar_width, alpha=opacity, color ='g', label='Recall')
	# plt.bar(index + bar_width, a, bar_width, alpha=opacity, color ='k', label='Accuracy')

	plt.title(title)
	plt.ylim([0, 1])
	plt.xlabel("Simulated Steps before Death")
	plt.ylabel("Percentage")
	plt.legend()
	plt.show()


def plt_pra(files, title=None, ylabel=None, xlabel='Epochs', cols=2):

	assert type(files) == str or type(files) == list, "need string or list for files arg"
	if type(files) == str:
		files = [files]


	styles = ['-', '--', 'r-', 'r--', 'b-', 'b--', 'g-', 'g--']
	legend = ['Train Accuracy', 'Test Accuracy', 'Train Precision', 'Test Precision', 'Train Recall', 'Test Recall']
	plt.figure(figsize=(10, 7), dpi=100, frameon=True, clear=False)

	for i, file in enumerate(files):
		data = np.loadtxt(file)

		x = np.arange(1, len(data) + 1)
		# x = np.arange(1, 36)

		for j in range(cols):

			try:
				style = styles[2*i+j]
			except OutOfRangeError:
				style = styles[len(styles)%2*i+j]

			# half = int(len(data) / 2)
			# if j==0:
			# 	y = data[:half]
			# else:
			# 	y = data[half:]

			y = data[:,j]

			plt.plot(x, y, style, label=legend[2*i+j])

		# plt.plot(x, data, style)



	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylim([0,1.0])
	plt.legend()
	plt.show()

def plt_pra2(files, title=None, ylabel=None, xlabel='Epochs', cols=2):
	# plots training and test pra
	assert type(files) == str or type(files) == list, "need string or list for files arg"
	if type(files) == str:
		files = [files]


	styles = ['-', '--', 'r-', 'r--', 'b-', 'b--', 'g-', 'g--']
	legend = ['Train Accuracy', 'Test Accuracy', 'Train Precision', 'Test Precision', 'Train Recall', 'Test Recall']
	plt.figure(figsize=(14,6), dpi=100, frameon=True, clear=False)

	ax1 = plt.subplot(121)
	ax2 = plt.subplot(122)
	for i, file in enumerate(files):
		data = np.loadtxt(file)

		x = np.arange(1, len(data) + 1)
		# x = np.arange(1, 36)

		for j in range(cols):

			try:
				style = styles[2*i+j]
			except OutOfRangeError:
				style = styles[len(styles)%2*i+j]

			# half = int(len(data) / 2)
			# if j==0:
			# 	y = data[:half]
			# else:
			# 	y = data[half:]

			y = data[:,j]
			if i == 0:
				ax1.plot(x, y, style, label=legend[2*i+j])
				ax1.legend()
				ax1.set_ylim([0,1.0])
				ax1.set_xlabel('Epochs')
				ax1.set_title('WM-Tiago MDN-RNN Accuracy,')
			else:
				ax2.plot(x, y, style, label=legend[2*i+j])
				ax2.legend()
				ax2.set_ylim([0,1.0])
				# ax2.set_title('Precision & Recall (weighted BCE, adj.7 steps prior done, 1000*d_loss)')

		# plt.plot(x, data, style)



	plt.title(title)
	plt.xlabel(xlabel)
	# plt.ylabel(ylabel)
	# # plt.ylim(top=0.2)
	# plt.legend()
	plt.show()


def plt_dat(files, title, ylabel, xlabel='Epochs'):
	# plot train and test losses
	assert type(files) == str or type(files) == list, "need string or list for files arg"
	if type(files) == str:
		files = [files]


	styles = ['bo', 'ro', '.', 'X', '+', '8', 'v']
	plt.figure(figsize=(8,6), dpi=100, frameon=True, clear=False)
	for i, file in enumerate(files):
		data = np.loadtxt(file)

		x = np.arange(1, len(data) + 1)

		try:
			style = styles[i]
		except OutOfRangeError:
			style = styles[len(styles)%i]

		plt.plot(x, data, style)



	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.show()



def plt_cols(file, title, ylabel, xlabel='Epochs', cols = 2):
	# plot loss
	data = np.loadtxt(file)
	x = np.arange(1, len(data) + 1)
	styles = ['-', '--', '.', 'X', '+', '8', 'v']
	legend = ['Train loss', 'Test loss']
	plt.figure(figsize=(12,6), dpi=100, frameon=True, clear=False)
	for i in range(cols):
		y = data[:,i]
		plt.plot(x, y, styles[i], label=legend[i])

	xint=range(min(x), max(x)+1)
	plt.xticks(xint)
	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.ylim(0, 0.1)
	plt.legend()
	plt.show()


if __name__ == "__main__":
	# plt_dat(['e10trainloss.dat', 'e10testloss.dat'], "Losses", "Avg batch loss")
	# plt_pra2(['twm_mdrnn_depthrelu3_acc.dat', 'twm_mdrnn_depthrelu3_prec.dat', 'twm_mdrnn_depthrelu3_rec.dat'], "Precision, & Recall for Done values", "P / R")
	# plt_cols('50twm_mdrnn_depthrelu3_mdrnnloss.dat', 'Mdrnn loss', 'Avg batch loss')
	# plot_dict('sim_twm_mdrnn_depth5st5soft_pra.npy', title="Done Prediction using Depth over Simulated Horizon (5 step soft prec.)")

	# avg_n_std_dict(["sim_twm_mdrnn_depth5st10soft2_pra.npy", "sim_twm_mdrnn_depth5st10soft3_pra.npy", "sim_twm_mdrnn_depth5st10soft4_pra.npy", "sim_twm_mdrnn_depth5st10soft_pra.npy"])
	# avg_n_std_list(["cr_data/cr_best0_rec.dat", "cr_data/cr_best1_rec.dat", "cr_data/cr_best2_rec.dat", "cr_data/cr_best3_rec.dat"])

	sim_final_plot(["sim_avg.npy"], ["sim_std.npy"],"Evaluation of Done Prediction over Simulated Steps using Depth Data")
	# pra_final_plot(["plots/base_rec_avgs.dat", "plots/best_rec_avgs.dat"], ["plots/base_rec_stds.dat", "plots/best_rec_stds.dat"])
