#!/usr/bin/env python
import torch
from torch import nn, optim, distributions
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split
from torchvision import transforms
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from torchvision import transforms, datasets
import numpy as np
import os
from PIL import Image
from tqdm import tqdm
import gc
import argparse
import scipy.misc

from models.vae import VAE
from models.mdnrnn import MDN_RNN, MDN_RNN_Cell

from evaluation import compare_ds, encode_obs
from utils.get_loader import get_loader
from utils.data_loader import CustomSeqDataset


batch_size = 1
seq_len=100
device = torch.device( "cuda" if torch.cuda.is_available() else "cpu")
toim = transforms.ToPILImage()
z_dim = 32

saved_mdrnn = 'saved/model_ckpts/twm_mdrnn_depthrelu4.pt' 
tagname = "twm_mdrnn_depth5st10soft4"
soft_precision = True
steps = 10

pred_first_done = True # only check the first done, and not subsequent
pred_xsteps = 5 # check first x dones, change x > 1 


action_size = 6
model_path = 'saved/model_ckpts/twm_vaedepth_relu.pt' # set model path


dataset = CustomSeqDataset('datasets/tiago_depth_test/', seq_len)

sim_loader = DataLoader(dataset, batch_size=batch_size, num_workers=0, shuffle=True, drop_last=True)


# instantiate models
vae = VAE().to(device)  
ckpt= torch.load(model_path)
vae.load_state_dict(ckpt['model_state_dict'])


mdrnncell= MDN_RNN_Cell(batch_size, seq_len, action_size = action_size, z_dim = z_dim).to(device)
ckpt= torch.load(saved_mdrnn)
state_dict = {k.strip('_l0'): v for k, v in ckpt['model_state_dict'].items()} # remove layer0 from key
mdrnncell.load_state_dict(state_dict)
mdrnncell.eval()

# compare w/ predictions of seq
mdrnn = MDN_RNN(batch_size, seq_len, action_size = action_size, z_dim = z_dim).to(device)
mdrnn.load_state_dict(ckpt['model_state_dict'])
mdrnn.eval()

print("Loaded MDN_RNN at epoch {} with test loss {}".format(ckpt['epoch'], ckpt['testloss']))

print("tiago simulation pred test using ", saved_mdrnn)
print("saved using tagname ", tagname)
if pred_first_done:
    print("predicting the first ", pred_xsteps, " steps for dones")

img_save_path = 'simulatetest/'
pbar = tqdm(total=len(sim_loader.dataset), desc="Simulating")

pra = {
    "total": 0,
    # "batch" : [0,0,0]
    # "90": [0,0,0],
    "40": [0,0,0], 
    "30": [0,0,0],
    "20": [0,0,0],
    "10": [0,0,0],
    "5" : [0,0,0],
    "2" : [0,0,0],
    "1" : [0,0,0]
}

pra_seq = [0, 0, 0]

def encode_obs(obs, next_obs=None, getboth=True):
    with torch.no_grad():
        zseq = [vae.forward_encode(seq).detach() for seq in obs] # [bs x seqlen], type [list, Tensor]
        z = torch.stack(zseq)
        del zseq

        if getboth:
            zseq_next = [vae.forward_encode(seq).detach() for seq in next_obs]
            z_next = torch.stack(zseq_next)
            del zseq_next

            return z, z_next
    return z


for i, batch in enumerate(sim_loader): 
    with torch.no_grad():
        obs, actions, reward, terminal, next_obs = [arr.to(device) for arr in batch]

        z, z_next = encode_obs(obs, next_obs)
        

        inputs = torch.cat([z, actions], dim=-1)
        pis, sigmas, mus, rs = mdrnn.forward(inputs)
        

        if not os.path.exists(img_save_path):
            os.mkdir(img_save_path)
        if not os.path.exists(img_save_path + 'hallucination'):
            os.mkdir(img_save_path + 'hallucination')


        checks = [40, 30, 20, 10, 5, 2, 1]

        ones_idx100 = np.flatnonzero(terminal[0].detach().cpu().numpy())

        ones_idx = []
        # only check if it gets the first done
        if pred_first_done:
            for i,v in enumerate(ones_idx100):
                if v - 1 >= 0: 
                    if terminal[0, v - 1] == 0: 
                        for step in range(pred_xsteps): 
                            if (i+step) < len(ones_idx100):
                                if ones_idx100[i + step] == v + step:
                                    ones_idx.append(ones_idx100[i + step])
                            # else:
                            #     print("only got up to step ", step)
        else:
            ones_idx = ones_idx100
    

        pra["total"] += len(ones_idx) 
  

        # for ea done, predict or sim & predict up to done idx
        for idx in ones_idx: 
            start = idx - 50
            if start < 0:
                pra["total"] -= 1
                continue
            
            ds_seq_real = terminal[0, start :idx+1].cpu().numpy()

            ds_seq_pred = np.empty_like(ds_seq_real)

            z_pred = mdrnn.get_next_z(pis, mus, sigmas)

            # get predictions based on depth img
            for i, seq in enumerate(z_pred[0, start :idx+1]):
                seq = seq.unsqueeze(0) # [1, 32]
                pred_img = vae.forward_decode(seq)
  
                img = np.array(pred_img.detach().cpu())
                low_counts = np.count_nonzero((img < 0.6) & (img > 0))
                
                if (np.count_nonzero(img == 0) > 4000) or  (low_counts > 200):

                    ds_seq_pred[i] = 1
                    
                else:
                    ds_seq_pred[i] = 0

            p, r, a, _t = compare_ds(ds_seq_pred, ds_seq_real, soft=soft_precision, steps=steps)
            pra_seq[0] += p; pra_seq[1] += r;  pra_seq[2] += a

            for st in checks: 

                # generated_images = []

                feed_idx = idx - st # feed real 

                ds_pred = []
                ds_real = terminal[0 , start : idx + 1 ].detach().cpu().numpy() # get corresponding terminal 


                # initial hidden & cell states
                h = torch.zeros(batch_size, 256, device=device)
                c = torch.zeros(batch_size, 256, device=device)


                # feed in real
                for ix in range(start, feed_idx): 
                    z = vae.forward_encode(obs[:, ix]) # [batch_size, 32] 
                    inp = torch.cat([z, actions[:, ix ]], dim=-1) # [batch_size, 35]

                    pi, sigma, mu, r, (h, c) = mdrnncell.forward(inp, (h, c))
                    
                    # save gen img
                    z_n = mdrnncell.get_next_z(pi, mu, sigma)

                    predictedim = vae.forward_decode(z_n)

            
                    # get predictions based on depth img
                    img_arr = np.array(predictedim.detach().cpu())
                    low_counts = np.count_nonzero((img_arr < 0.6) & (img_arr > 0))
                
                    if (np.count_nonzero(img_arr == 0) > 4000) or  (low_counts > 200):

                        ds_pred.append(1)
                        
                    else:
                        ds_pred.append(0)

                    # img = toim(predictedim[0].detach().cpu())
                    # img = img.resize((320,240), Image.ANTIALIAS)
                    # generated_images.append(img)

                    # img.save('mispred_sim/batch' + str(i) + '_' + str(idx) + 'gen.jpg')
                    

                # simulate
                z_next = vae.forward_encode(obs[:, feed_idx])

                for j in range(feed_idx, idx+1 ): 
                    inp = torch.cat([z_next, actions[:,j]], dim=-1)

                    pi, sigma, mu, r, (h, c) = mdrnncell.forward(inp, (h, c))
            

                    z_next = mdrnncell.get_next_z(pi, mu, sigma) # [1, 32]

                    # save img
                    predictedimg = vae.forward_decode(z_next) # [1, imgsize]
                    # get predictions based on depth img
                    img_arr = np.array(predictedimg.detach().cpu())
                    low_counts = np.count_nonzero((img_arr < 0.6) & (img_arr > 0))
                
                    if (np.count_nonzero(img_arr == 0) > 4000) or  (low_counts > 200):

                        ds_pred.append(1)
                        
                    else:
                        ds_pred.append(0)

                    # img = toim(predictedimg[0].detach().cpu())
                    # img = img.resize((320,240), Image.ANTIALIAS)
                    # generated_images.append(img)


                    # # img.save('mispred_sim/batch' + str(i) + '_' + str(j) + 'sim.jpg')

                

                p, r, a, _t = compare_ds(ds_pred, ds_real, soft= soft_precision, steps=steps)

                    # examine mispredicted for sim 10 steps 
                    # for k, dp in enumerate(ds_pred):
                    #     for j, dr in enumerate(ds_real):
                    #         pred = int(dp >= 0.5)
                    #         real = int(dr)
                    #         if pred != real: 
                    #             real_im = obs.cpu().detach()[0, k]
                    #             real_im = np.transpose(real_im, (1,2,0))
                    #             scipy.misc.imsave('mispred_sim/real/batch' + str(i) + 'step' + str(k) + 'pred' + str(pred) + 'true' + str(real) + '.jpg', real_im)
                    #             gen_im = generated_images[k]
                    #             scipy.misc.imsave('mispred_sim/batch' + str(i) + 'step' + str(k) + 'pred' + str(pred) + 'true' + str(real) + '.jpg', gen_im)

                                


                # print(np.flatnonzero(ds_real))
                # print(np.round(ds_pred[-5:], 3))

                pra[str(st)][0] += p ; pra[str(st)][1] += r ; pra[str(st)][2] += a

        

        pbar.update(batch_size)

pbar.close()

print("total seq: ", pra["total"])

pra_seq = [ p / pra["total"] for p in pra_seq]

pra = {k: np.array(v)/ pra["total"] for k, v in pra.items() if k != "total"} 

for k, v in pra.items():
    print(k, ": ", v)

print('seq pra: ', pra_seq)

np.savetxt("simseq_" + tagname + "_pra.dat", pra_seq)
np.save("sim_" + tagname + "_pra.npy", pra)
    
